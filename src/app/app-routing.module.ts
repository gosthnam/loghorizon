import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MainPageComponent} from '@components/main-page/main-page.component';
import { SkillsComponent } from '@components/skills/skills.component';

const routes: Routes = [
  {path: '', component: MainPageComponent},
  {path: 'skills-component', component: SkillsComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
