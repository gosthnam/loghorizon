import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { SkillsFilterPipe } from '@shared/skillsFilter/skills-filter.pipe';

import { SkillsComponent } from '@components/skills/skills.component';
import { SkillComponent } from '@components/skill/skill.component';
import { MainPageComponent } from '@components/main-page/main-page.component';

@NgModule({
  declarations: [
    AppComponent,
    SkillsComponent,
    SkillComponent,
    MainPageComponent,
    SkillsFilterPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
