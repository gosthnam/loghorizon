import { Component, OnInit } from '@angular/core';

import skillsJson from '@assets/skills.json';
import { Skill } from '@shared/skill';

@Component({
  selector: 'app-skills',
  templateUrl: './skills.component.html',
  styleUrls: ['./skills.component.scss']
})
export class SkillsComponent implements OnInit {

  display: boolean;
  skills: Skill[];
  filteredSkills: Skill[];
  skillFilter: string;

  constructor() { 
  }

  ngOnInit(): void {
    this.display = true;
    this.skills = skillsJson["skills"];
    this.filteredSkills = this.skills;
    this.skillFilter = "";
  }

}
