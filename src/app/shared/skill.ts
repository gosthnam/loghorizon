export interface Skill {
    name: string;
    skillType: string;
    tag: string[];
    maxSR: number;
    timing: string;
    check: string;
    target: string;
    range: string;
    cost: string;
    limit: string;
    description: string;
}