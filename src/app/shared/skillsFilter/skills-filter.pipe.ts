import { Pipe, PipeTransform } from '@angular/core';
import { Skill } from '@shared/skill';

@Pipe({
  name: 'skillsFilter'
})
export class SkillsFilterPipe implements PipeTransform {

  transform(skills: Skill[], input: string): Skill[] {
    if (input !== undefined && input !== null && input !== "") {
      return skills.filter((skill: Skill) => skill.name.toLowerCase().includes(input.toLowerCase()) || skill.description.toLowerCase().includes(input.toLowerCase()));
    } else {
      return skills;
    }
  }

}
